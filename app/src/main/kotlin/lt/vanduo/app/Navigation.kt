/*
 * Vanduo app to locate water springs.
 * Copyright© 2023-2024
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

package lt.vanduo.app

import android.app.Activity
import android.widget.Toast
import androidx.activity.compose.BackHandler
import androidx.compose.animation.AnimatedContentTransitionScope
import androidx.compose.animation.core.EaseIn
import androidx.compose.animation.core.EaseOut
import androidx.compose.animation.core.tween
import androidx.compose.animation.fadeIn
import androidx.compose.animation.fadeOut
import androidx.compose.animation.scaleIn
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableLongStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.platform.LocalContext
import androidx.lifecycle.viewmodel.compose.viewModel
import androidx.navigation.NavHostController
import androidx.navigation.NavType
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import androidx.navigation.navArgument
import lt.vanduo.app.Destinations.INFO_SCREEN
import lt.vanduo.app.Destinations.SPLASH_SCREEN
import lt.vanduo.app.Destinations.SPRINGS_SCREEN
import lt.vanduo.app.Destinations.SPRING_DETAIL_SCREEN
import lt.vanduo.app.info.InfoScreen
import lt.vanduo.app.splash.SplashScreen
import lt.vanduo.app.splash.SplashViewModel
import lt.vanduo.app.splash.SplashViewModelFactory
import lt.vanduo.app.springs.SpringDetailScreen
import lt.vanduo.app.springs.SpringsScreen
import lt.vanduo.app.springs.SpringsViewModel
import lt.vanduo.app.springs.SpringsViewModelFactory
import lt.vanduo.app.utils.Country
import lt.vanduo.app.utils.Preferences

object Destinations {
    const val SPLASH_SCREEN = "SPLASH_SCREEN"
    const val SPRINGS_SCREEN = "SPRINGS_SCREEN"
    const val INFO_SCREEN = "INFO_SCREEN"
    const val SPRING_NAME_ID = "springNameId"
    const val SPRING_DETAIL_SCREEN = "SPRING_DETAIL_SCREEN"
}

@Composable
fun VanduoNavHost(navController: NavHostController = rememberNavController()) {
    val context = LocalContext.current
    val country = Country(Preferences(context))
    NavHost(
        navController = navController,
        startDestination = SPLASH_SCREEN,
    ) {

        composable(SPLASH_SCREEN) {
            val viewModel: SplashViewModel = viewModel(
                factory = SplashViewModelFactory()
            )

            SplashScreen(
                viewModel,
            ) { navController.navigate(SPRINGS_SCREEN) }
        }

        composable(SPRINGS_SCREEN) {
            DoubleBackHandler()
            val viewModel: SpringsViewModel = viewModel(
                factory = SpringsViewModelFactory(country)
            )
            SpringsScreen(
                viewModel,
                navigateToSpringDetailScreen = {
                    navController.navigate(SPRING_DETAIL_SCREEN + "/${it.nameId}")
                },
                navigateToInfoScreen = { navController.navigate(INFO_SCREEN) },
            )
        }

        composable(
            SPRING_DETAIL_SCREEN + "/{${Destinations.SPRING_NAME_ID}}",
            arguments = listOf(navArgument(Destinations.SPRING_NAME_ID) { type = NavType.IntType }),
            enterTransition = { fadeIn() + scaleIn() },
            exitTransition = {
                fadeOut() + slideOutOfContainer(
                    animationSpec = tween(250, easing = EaseOut),
                    towards = AnimatedContentTransitionScope.SlideDirection.End
                )
            }
        ) {
            SpringDetailScreen(it.arguments?.getInt(Destinations.SPRING_NAME_ID))
        }

        composable(
            route = INFO_SCREEN,
            enterTransition = {
                fadeIn() + slideIntoContainer(
                    animationSpec = tween(250, easing = EaseIn),
                    towards = AnimatedContentTransitionScope.SlideDirection.Up
                )
            },
            exitTransition = {
                fadeOut() + slideOutOfContainer(
                    animationSpec = tween(250, easing = EaseOut),
                    towards = AnimatedContentTransitionScope.SlideDirection.Down
                )
            }
        ) {
            InfoScreen(country)
        }
    }
}

@Composable
private fun DoubleBackHandler() {
    var lastPressTime by remember { mutableLongStateOf(0L) }
    val context = LocalContext.current

    BackHandler(true) {
        val timeNow = System.currentTimeMillis()
        if (lastPressTime + 2000 < timeNow) {
            lastPressTime = timeNow
            Toast.makeText(context, R.string.toast_back_press, Toast.LENGTH_SHORT)
                .show()
        } else {
            (context as? Activity)?.finish()
        }
    }
}