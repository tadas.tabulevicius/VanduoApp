/*
 * Vanduo app to locate water springs.
 * Copyright© 2023-2024
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

package lt.vanduo.app.utils

import androidx.annotation.StringRes
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.launch
import lt.vanduo.app.R

class Country(
    private val prefs: Preferences,
) {
    sealed class Code(
        val isoName: String,
        @StringRes val titleId: Int,
    ) {
        object ALL : Code("all", R.string.country_title_all)
        object LT : Code("lt", R.string.country_title_lt)
        object IN : Code("in", R.string.country_title_in)
        object LK : Code("lk", R.string.country_title_lk)
        object MY : Code("my", R.string.country_title_my)
        object TH : Code("th", R.string.country_title_th)
        object SG : Code("sg", R.string.country_title_sg)
        object CN : Code("cn", R.string.country_title_cn)
        object VN : Code("vn", R.string.country_title_vn)
    }

    val options = listOf(
        Code.ALL,
        Code.LT,
        Code.LK,
        Code.MY,
        Code.TH,
        Code.IN,
        Code.SG,
        Code.CN,
        Code.VN,
    )
    val selectedCountryCodeFlow = MutableSharedFlow<Code>().apply { tryEmit(getSelectedCode()) }

    fun getSelectedCode() = when (prefs.getCountry()) {
        Code.LT.isoName -> Code.LT
        Code.IN.isoName -> Code.IN
        Code.LK.isoName -> Code.LK
        Code.MY.isoName -> Code.MY
        Code.TH.isoName -> Code.TH
        Code.SG.isoName -> Code.SG
        Code.CN.isoName -> Code.CN
        Code.VN.isoName -> Code.VN
        else -> Code.ALL
    }

    fun select(code: Code) {
        if (prefs.getCountry() == code.isoName) {
            return
        }

        prefs.setCountry(code.isoName)

        kotlinx.coroutines.MainScope().launch { selectedCountryCodeFlow.emit(code) }
    }
}