/*
 * Vanduo app to locate water springs.
 * Copyright© 2023-2024
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

package lt.vanduo.app.utils

import android.app.Activity
import android.content.res.Configuration
import androidx.annotation.StringRes
import lt.vanduo.app.R

import java.util.Locale

class Language(
    private val activity: Activity,
    private val prefs: Preferences,
) {
    sealed class Code(
        val isoName: String,
        @StringRes val titleId: Int,
    ) {
        object LT : Code("lt", R.string.language_title_lt)
        object EN : Code("en", R.string.language_title_en)
    }

    val options = listOf(Code.LT, Code.EN)

    fun check() {
        val languageName = prefs.getLanguage()

        when (languageName) {
            getDefaultCode().isoName -> return
            Code.LT.isoName -> setNewLocale(Code.LT.isoName)
            Code.EN.isoName -> setNewLocale(Code.EN.isoName)
            else -> setNewLocale(getDefaultCode().isoName)
        }
    }

    fun getDefaultCode() = when (Locale.getDefault().language) {
        Code.LT.isoName -> Code.LT
        else -> Code.EN
    }

    fun select(code: Code) {
        if (Locale.getDefault().language == code.isoName) {
            return
        }

        val newCode = code.isoName
        setNewLocale(newCode)

        activity.recreate()
    }

    private fun setNewLocale(isoCode: String) {
        activity.resources.apply {

            val locale = Locale(isoCode)
            val config = Configuration(configuration)

            activity.createConfigurationContext(configuration)
            Locale.setDefault(locale)
            config.setLocale(locale)

            @Suppress("DEPRECATION")
            updateConfiguration(config, displayMetrics)

            prefs.setLanguage(isoCode)
        }
    }
}