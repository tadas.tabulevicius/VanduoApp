/*
 * Vanduo app to locate water springs.
 * Copyright© 2023-2024
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

package lt.vanduo.app.utils

import android.content.Context
import android.content.SharedPreferences

class Preferences(private val context: Context) {

    private val sharedPreferences: SharedPreferences by lazy {
        context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE)
    }

    private fun putString(key: String, value: String) {
        sharedPreferences.edit().putString(key, value).apply()
    }

    private fun getString(key: String, defaultValue: String? = null): String? {
        return sharedPreferences.getString(key, defaultValue)
    }

    fun getLanguage(): String? {
        return getString(PREF_KEY_LANGUAGE)
    }

    fun setLanguage(languageCode: String) {
        putString(PREF_KEY_LANGUAGE, languageCode)
    }

    fun getCountry(): String? {
        return getString(PREF_KEY_COUNTRY)
    }

    fun setCountry(countryCode: String) {
        putString(PREF_KEY_COUNTRY, countryCode)
    }

    companion object {
        private const val PREFS_NAME = "VanduoAppPrefs"
        private const val PREF_KEY_LANGUAGE = "PREF_KEY_LANGUAGE"
        private const val PREF_KEY_COUNTRY = "PREF_KEY_COUNTRY"
    }
}