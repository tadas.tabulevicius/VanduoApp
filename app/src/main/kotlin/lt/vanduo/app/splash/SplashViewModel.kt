/*
 * Vanduo app to locate water springs.
 * Copyright© 2023-2024
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

package lt.vanduo.app.splash

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow

class SplashViewModel : ViewModel() {
    val animationDataFlow: Flow<SplashScreenData> = flow {
        val frameInterval = 70L

        delay(frameInterval)
        emit(SplashScreenData(positionOfDot = 1))
        delay(frameInterval)
        emit(SplashScreenData(positionOfDot = 2))
        delay(frameInterval)
        emit(SplashScreenData(positionOfDot = 3))
        delay(frameInterval)
        emit(SplashScreenData(positionOfDot = 4))
        delay(frameInterval)
        emit(SplashScreenData(positionOfDot = 5))
        delay(frameInterval)
        emit(SplashScreenData(positionOfHex = 5))
        delay(frameInterval)
        emit(SplashScreenData(positionOfHexO = 5))
        delay(frameInterval)
        emit(SplashScreenData(positionOfO = 5))

        delay(frameInterval)
        emit(
            SplashScreenData(
                positionOfDot = 1,
                positionOfO = 5,
            )
        )
        delay(frameInterval)
        emit(
            SplashScreenData(
                positionOfDot = 2,
                positionOfO = 5,
            )
        )
        delay(frameInterval)
        emit(
            SplashScreenData(
                positionOfDot = 3,
                positionOfO = 5,
            )
        )
        delay(frameInterval)
        emit(
            SplashScreenData(
                positionOfDot = 4,
                positionOfO = 5,
            )
        )
        delay(frameInterval)
        emit(
            SplashScreenData(
                positionOfHex = 4,
                positionOfO = 5,
            )
        )
        delay(frameInterval)
        emit(
            SplashScreenData(
                positionOfHexO = 4,
                positionOfO = 5,
            )
        )
        delay(frameInterval)
        emit(
            SplashScreenData(
                positionOfU = 4,
                positionOfO = 5,
            )
        )

        delay(frameInterval)
        emit(
            SplashScreenData(
                positionOfDot = 1,
                positionOfU = 4,
                positionOfO = 5,
            )
        )
        delay(frameInterval)
        emit(
            SplashScreenData(
                positionOfDot = 2,
                positionOfU = 4,
                positionOfO = 5,
            )
        )
        delay(frameInterval)
        emit(
            SplashScreenData(
                positionOfDot = 3,
                positionOfU = 4,
                positionOfO = 5,
            )
        )
        delay(frameInterval)
        emit(
            SplashScreenData(
                positionOfHex = 3,
                positionOfU = 4,
                positionOfO = 5,
            )
        )
        delay(frameInterval)
        emit(
            SplashScreenData(
                positionOfHexO = 3,
                positionOfU = 4,
                positionOfO = 5,
            )
        )
        delay(frameInterval)
        emit(
            SplashScreenData(
                positionOfD = 3,
                positionOfU = 4,
                positionOfO = 5,
            )
        )

        delay(frameInterval)
        emit(
            SplashScreenData(
                positionOfDot = 1,
                positionOfD = 3,
                positionOfU = 4,
                positionOfO = 5,
            )
        )
        delay(frameInterval)
        emit(
            SplashScreenData(
                positionOfDot = 2,
                positionOfD = 3,
                positionOfU = 4,
                positionOfO = 5,
            )
        )
        delay(frameInterval)
        emit(
            SplashScreenData(
                positionOfHex = 2,
                positionOfD = 3,
                positionOfU = 4,
                positionOfO = 5,
            )
        )
        delay(frameInterval)
        emit(
            SplashScreenData(
                positionOfHexO = 2,
                positionOfD = 3,
                positionOfU = 4,
                positionOfO = 5,
            )
        )
        delay(frameInterval)
        emit(
            SplashScreenData(
                positionOfN = 2,
                positionOfD = 3,
                positionOfU = 4,
                positionOfO = 5,
            )
        )

        delay(frameInterval)
        emit(
            SplashScreenData(
                positionOfDot = 1,
                positionOfN = 2,
                positionOfD = 3,
                positionOfU = 4,
                positionOfO = 5,
            )
        )
        delay(frameInterval)
        emit(
            SplashScreenData(
                positionOfHex = 1,
                positionOfN = 2,
                positionOfD = 3,
                positionOfU = 4,
                positionOfO = 5,
            )
        )
        delay(frameInterval)
        emit(
            SplashScreenData(
                positionOfHexO = 1,
                positionOfN = 2,
                positionOfD = 3,
                positionOfU = 4,
                positionOfO = 5,
            )
        )
        delay(frameInterval)
        emit(
            SplashScreenData(
                positionOfA = 1,
                positionOfN = 2,
                positionOfD = 3,
                positionOfU = 4,
                positionOfO = 5,
            )
        )
    }
}

class SplashViewModelFactory : ViewModelProvider.Factory {
    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(SplashViewModel::class.java)) {
            return SplashViewModel() as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}

data class SplashScreenData(
    var positionOfA: Int = 0,
    var positionOfN: Int = 0,
    var positionOfD: Int = 0,
    var positionOfU: Int = 0,
    var positionOfO: Int = 0,
    var positionOfDot: Int = 0,
    var positionOfHex: Int = 0,
    var positionOfHexO: Int = 0,
)




