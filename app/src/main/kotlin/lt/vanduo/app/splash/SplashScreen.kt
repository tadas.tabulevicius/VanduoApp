/*
 * Vanduo app to locate water springs.
 * Copyright© 2023-2024
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

package lt.vanduo.app.splash

import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.height
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.lifecycle.compose.collectAsStateWithLifecycle
import kotlinx.coroutines.delay
import lt.vanduo.app.R
import lt.vanduo.app.ui.theme.Blue

@Composable
fun SplashScreen(
    viewModel: SplashViewModel,
    navigateToSpringsScreen: () -> Unit
) {
    LaunchedEffect(Unit) {
        delay(2900)
        navigateToSpringsScreen()
    }

    Surface(
        modifier = Modifier.fillMaxSize(),
        color = MaterialTheme.colorScheme.background,
    ) {
        Column(
            verticalArrangement = Arrangement.Center,
            horizontalAlignment = Alignment.CenterHorizontally,
        ) {
            val animationData by viewModel.animationDataFlow.collectAsStateWithLifecycle(
                initialValue = SplashScreenData()
            )
            Image(
                painter = painterResource(id = R.drawable.ic_inv_triangle_48),
                contentDescription = null,
            )
            ChangingSymbol(
                position = 1,
                animationData,
            )
            ChangingSymbol(
                position = 2,
                animationData,
            )
            ChangingSymbol(
                position = 3,
                animationData,
            )
            ChangingSymbol(
                position = 4,
                animationData,
            )
            ChangingSymbol(
                position = 5,
                animationData,
            )
        }
    }
}

@Composable
private fun ChangingSymbol(
    position: Int,
    data: SplashScreenData,
) {
    when (position) {
        data.positionOfDot -> BlueDot()
        data.positionOfHex -> BlueHex()
        else -> Text(
            text = when (position) {
                data.positionOfA -> "a"
                data.positionOfN -> "n"
                data.positionOfD -> "d"
                data.positionOfU -> "u"
                data.positionOfO -> "o"
                data.positionOfHexO -> "⬡"
                else -> ""
            },
            style = MaterialTheme.typography.titleLarge,
            modifier = Modifier.height(symbolHeight)
        )
    }
}

@Composable
private fun BlueDot() {
    Text(
        text = "•",
        color = Blue,
        style = MaterialTheme.typography.titleLarge,
        modifier = Modifier.height(symbolHeight)
    )
}

@Composable
private fun BlueHex() {
    Text(
        text = "⬢",
        color = Blue,
        style = MaterialTheme.typography.titleLarge,
        modifier = Modifier.height(symbolHeight)
    )
}

@Preview
@Composable
fun SplashPreview() {
    SplashScreen(viewModel = SplashViewModel()) { }
}

val symbolHeight = 32.dp