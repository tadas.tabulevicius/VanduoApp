/*
 * Vanduo app to locate water springs.
 * Copyright© 2023-2024
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

package lt.vanduo.app.info

import android.app.Activity
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.selection.selectable
import androidx.compose.foundation.text.ClickableText
import androidx.compose.foundation.text.selection.SelectionContainer
import androidx.compose.foundation.verticalScroll
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.RadioButton
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.platform.LocalUriHandler
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.semantics.Role
import androidx.compose.ui.text.AnnotatedString
import androidx.compose.ui.text.SpanStyle
import androidx.compose.ui.unit.dp
import lt.vanduo.app.BuildConfig
import lt.vanduo.app.R
import lt.vanduo.app.utils.Country
import lt.vanduo.app.utils.Language
import lt.vanduo.app.utils.Preferences

@Composable
fun InfoScreen(country: Country) {
    Surface(
        modifier = Modifier.fillMaxSize(),
        color = MaterialTheme.colorScheme.background,
    ) {
        Column(
            modifier = Modifier.verticalScroll(rememberScrollState())
        ) {
            LanguageSelection()

            CountrySelection(country)

            Text(
                text = stringResource(R.string.version, BuildConfig.VERSION_NAME),
                style = MaterialTheme.typography.titleMedium,
                modifier = Modifier.padding(16.dp)
            )

            Text(
                text = stringResource(R.string.license),
                style = MaterialTheme.typography.bodyMedium,
                modifier = Modifier.padding(16.dp)
            )

            Text(
                text = stringResource(R.string.code_title),
                style = MaterialTheme.typography.titleMedium,
                modifier = Modifier.padding(
                    start = 16.dp,
                    top = 16.dp,
                    end = 16.dp,
                    bottom = 8.dp
                )
            )

            val sourceUrl = stringResource(R.string.code_url)
            val uriHandler = LocalUriHandler.current
            SelectionContainer {
                val smallTitleStyle = MaterialTheme.typography.titleSmall
                ClickableText(
                    text = AnnotatedString(
                        sourceUrl,
                        SpanStyle(
                            MaterialTheme.colorScheme.primary,
                            smallTitleStyle.fontSize,
                            smallTitleStyle.fontWeight,
                            smallTitleStyle.fontStyle
                        )
                    ),
                    onClick = { uriHandler.openUri(sourceUrl) },
                    modifier = Modifier.padding(
                        start = 16.dp,
                        top = 0.dp,
                        end = 16.dp,
                        bottom = 16.dp
                    )
                )
            }
        }
    }
}

@Composable
private fun LanguageSelection() {
    val context = LocalContext.current
    val language = Language(context as Activity, Preferences(context))
    var selected = language.getDefaultCode()

    Row(Modifier.padding(16.dp)) {
        Text(
            text = stringResource(R.string.language_title, BuildConfig.VERSION_NAME),
            style = MaterialTheme.typography.titleMedium,
            modifier = Modifier.padding(top = 12.dp),
        )
        Column(modifier = Modifier.padding(horizontal = 8.dp)) {
            for (lang in language.options) {
                Row(
                    Modifier
                        .fillMaxWidth()
                        .height(48.dp)
                        .selectable(
                            selected = (lang == selected),
                            onClick = {
                                selected = lang
                                language.select(selected)
                            },
                            role = Role.RadioButton
                        ),
                    verticalAlignment = Alignment.CenterVertically
                ) {
                    RadioButton(
                        selected = (lang == selected),
                        onClick = null,
                        modifier = Modifier.padding(horizontal = 16.dp)
                    )
                    Text(
                        text = stringResource(lang.titleId),
                        style = MaterialTheme.typography.bodyLarge,
                    )
                }
            }
        }
    }
}

@Composable
private fun CountrySelection(country: Country) {
    var selected by remember { mutableStateOf(country.getSelectedCode()) }

    Row(Modifier.padding(16.dp)) {
        Text(
            text = stringResource(R.string.country_title, BuildConfig.VERSION_NAME),
            style = MaterialTheme.typography.titleMedium,
            modifier = Modifier.padding(top = 12.dp),
        )
        Column(modifier = Modifier.padding(horizontal = 8.dp)) {
            for (countryCode in country.options) {
                Row(
                    Modifier
                        .fillMaxWidth()
                        .height(48.dp)
                        .selectable(
                            selected = (countryCode == selected),
                            onClick = {
                                selected = countryCode
                                country.select(selected)
                            },
                            role = Role.RadioButton
                        ),
                    verticalAlignment = Alignment.CenterVertically
                ) {
                    RadioButton(
                        selected = (countryCode == selected),
                        onClick = null,
                        modifier = Modifier.padding(horizontal = 16.dp)
                    )
                    Text(
                        text = stringResource(countryCode.titleId),
                        style = MaterialTheme.typography.bodyLarge,
                    )
                }
            }
        }
    }
}
