/*
 * Vanduo app to locate water springs.
 * Copyright© 2023-2024
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

package lt.vanduo.app.springs

import androidx.annotation.DrawableRes
import androidx.annotation.StringRes
import lt.vanduo.app.R
import lt.vanduo.app.utils.Country

data class Spring(
    @StringRes val nameId: Int,
    @StringRes val descriptionId: Int,
    @StringRes val geoLocationUriId: Int,
    @DrawableRes val imageResourceId: Int? = null,
)

data class Location(
    @StringRes val nameId: Int,
    val country: Country.Code,
    val springs: List<Spring>,
)

val springsVilnius = listOf(
    Spring(
        nameId = R.string.spring_saltupis_name,
        descriptionId = R.string.spring_saltupis_description,
        geoLocationUriId = R.string.spring_saltupis_geo_uri,
        imageResourceId = R.drawable.img_saltupis,
    ),
    Spring(
        nameId = R.string.spring_cave_name,
        descriptionId = R.string.spring_cave_description,
        geoLocationUriId = R.string.spring_cave_geo_uri,
        imageResourceId = R.drawable.img_cave,
    ),
    Spring(
        nameId = R.string.spring_zverynas_name,
        descriptionId = R.string.spring_zverynas_description,
        geoLocationUriId = R.string.spring_zverynas_geo_uri,
        imageResourceId = R.drawable.img_zverynas,
    ),
    Spring(
        nameId = R.string.spring_valley_name,
        descriptionId = R.string.spring_valley_description,
        geoLocationUriId = R.string.spring_raguvos_geo_uri,
        imageResourceId = R.drawable.img_valley,
    ),
    Spring(
        nameId = R.string.spring_raguvos_name,
        descriptionId = R.string.spring_raguvos_description,
        geoLocationUriId = R.string.spring_raguvos_geo_uri,
        imageResourceId = R.drawable.img_raguvos,
    ),
    Spring(
        nameId = R.string.spring_snipiskes_name,
        descriptionId = R.string.spring_snipiskes_description,
        geoLocationUriId = R.string.spring_snipiskes_geo_uri,
        imageResourceId = R.drawable.img_snipiskes,
    ),
    Spring(
        nameId = R.string.spring_vileisis_name,
        descriptionId = R.string.spring_vileisis_description,
        geoLocationUriId = R.string.spring_vileisis_geo_uri,
        imageResourceId = R.drawable.img_vileisis,
    ),
    Spring(
        nameId = R.string.spring_colorful_name,
        descriptionId = R.string.spring_colorful_description,
        geoLocationUriId = R.string.spring_colorful_geo_uri,
        imageResourceId = R.drawable.img_springs_colorful,
    ),
    Spring(
        nameId = R.string.spring_fabijoniskes_name,
        descriptionId = R.string.spring_fabijoniskes_description,
        geoLocationUriId = R.string.spring_fabijoniskes_geo_uri,
        imageResourceId = R.drawable.img_fabijoniskes,
    ),
    Spring(
        nameId = R.string.spring_karoliniskes_name,
        descriptionId = R.string.spring_karoliniskes_description,
        geoLocationUriId = R.string.spring_karoliniskes_geo_uri,
        imageResourceId = R.drawable.img_karoliniskes,
    ),
    Spring(
        nameId = R.string.spring_dvarcionys_name,
        descriptionId = R.string.spring_dvarcionys_description,
        geoLocationUriId = R.string.spring_dvarcionys_geo_uri,
        imageResourceId = R.drawable.img_dvarcionys,
    ),
    Spring(
        nameId = R.string.spring_verkiai_name,
        descriptionId = R.string.spring_verkiai_description,
        geoLocationUriId = R.string.spring_verkiai_geo_uri,
        imageResourceId = R.drawable.img_verkiai_1,
    ),
    Spring(
        nameId = R.string.spring_happiness_name,
        descriptionId = R.string.spring_happiness_description,
        geoLocationUriId = R.string.spring_happiness_geo_uri,
        imageResourceId = R.drawable.img_happiness,
    ),
    Spring(
        nameId = R.string.spring_abromiskes_name,
        descriptionId = R.string.spring_abromiskes_description,
        geoLocationUriId = R.string.spring_abromiskes_geo_uri,
        imageResourceId = R.drawable.img_abromiskes,
    ),
)

val springsKaunas = listOf(
    Spring(
        nameId = R.string.spring_parakas_name,
        descriptionId = R.string.spring_parakas_description,
        geoLocationUriId = R.string.spring_parakas_geo_uri,
        imageResourceId = R.drawable.img_parakas,
    ),
    Spring(
        nameId = R.string.spring_linksme_name,
        descriptionId = R.string.spring_linksme_description,
        geoLocationUriId = R.string.spring_linksme_geo_uri,
        imageResourceId = R.drawable.img_linksme,
    ),
    Spring(
        nameId = R.string.spring_kleboniskis_1_name,
        descriptionId = R.string.spring_kleboniskis_1_description,
        geoLocationUriId = R.string.spring_kleboniskis_1_geo_uri,
        imageResourceId = R.drawable.img_kleboniskis_1,
    ),
    Spring(
        nameId = R.string.spring_kleboniskis_2_name,
        descriptionId = R.string.spring_kleboniskis_2_description,
        geoLocationUriId = R.string.spring_kleboniskis_2_geo_uri,
        imageResourceId = R.drawable.img_kleboniskis_2,
    ),
    Spring(
        nameId = R.string.spring_kaniukai_name,
        descriptionId = R.string.spring_kaniukai_description,
        geoLocationUriId = R.string.spring_kaniukai_geo_uri,
        imageResourceId = R.drawable.img_kaniukai,
    ),
    Spring(
        nameId = R.string.spring_radikiai_name,
        descriptionId = R.string.spring_radikiai_description,
        geoLocationUriId = R.string.spring_radikiai_geo_uri,
        imageResourceId = R.drawable.img_radikiai,
    ),
    Spring(
        nameId = R.string.spring_zaisa_name,
        descriptionId = R.string.spring_zaisa_description,
        geoLocationUriId = R.string.spring_zaisa_geo_uri,
        imageResourceId = R.drawable.img_zaisa,
    ),
    Spring(
        nameId = R.string.spring_kacergine_1_name,
        descriptionId = R.string.spring_kacergine_1_description,
        geoLocationUriId = R.string.spring_kacergine_1_geo_uri,
        imageResourceId = R.drawable.img_kacergine_1,
    ),
    Spring(
        nameId = R.string.spring_kacergine_2_name,
        descriptionId = R.string.spring_kacergine_2_description,
        geoLocationUriId = R.string.spring_kacergine_2_geo_uri,
        imageResourceId = R.drawable.img_kacergine_2,
    ),
    Spring(
        nameId = R.string.spring_kacergine_3_name,
        descriptionId = R.string.spring_kacergine_3_description,
        geoLocationUriId = R.string.spring_kacergine_3_geo_uri,
        imageResourceId = R.drawable.img_kacergine_3,
    ),
    Spring(
        nameId = R.string.spring_love_name,
        descriptionId = R.string.spring_love_description,
        geoLocationUriId = R.string.spring_love_geo_uri,
        imageResourceId = R.drawable.img_love,
    ),
    Spring(
        nameId = R.string.spring_st_john_name,
        descriptionId = R.string.spring_st_john_description,
        geoLocationUriId = R.string.spring_st_john_geo_uri,
        imageResourceId = R.drawable.img_st_john,
    ),
)

val springsTaurage = listOf(
    Spring(
        nameId = R.string.spring_papusyne_name,
        descriptionId = R.string.spring_papusyne_description,
        geoLocationUriId = R.string.spring_papusyne_geo_uri,
        imageResourceId = R.drawable.img_papusyne,
    ),
    Spring(
        nameId = R.string.spring_didlaukis_name,
        descriptionId = R.string.spring_didlaukis_description,
        geoLocationUriId = R.string.spring_didlaukis_geo_uri,
        imageResourceId = R.drawable.img_didlaukis,
    ),
    Spring(
        nameId = R.string.spring_eiciai_name,
        descriptionId = R.string.spring_eiciai_description,
        geoLocationUriId = R.string.spring_eiciai_geo_uri,
        imageResourceId = R.drawable.img_eiciai,
    ),
)

val springsJurbarkas = listOf(
    Spring(
        nameId = R.string.spring_greiciai_name,
        descriptionId = R.string.spring_greiciai_description,
        geoLocationUriId = R.string.spring_greiciai_geo_uri,
        imageResourceId = R.drawable.img_greiciai,
    ),
    Spring(
        nameId = R.string.spring_joniniu_name,
        descriptionId = R.string.spring_joniniu_description,
        geoLocationUriId = R.string.spring_joniniu_geo_uri,
        imageResourceId = R.drawable.img_joniniu,
    ),
)

val springsSeredzius = listOf(
    Spring(
        nameId = R.string.spring_seredzius_name,
        descriptionId = R.string.spring_seredzius_description,
        geoLocationUriId = R.string.spring_seredzius_geo_uri,
        imageResourceId = R.drawable.img_seredzius,
    ),
    Spring(
        nameId = R.string.spring_palazduonis_name,
        descriptionId = R.string.spring_palazduonis_description,
        geoLocationUriId = R.string.spring_palazduonis_geo_uri,
        imageResourceId = R.drawable.img_palazduonis,
    ),
)

val springsAukstadvaris = listOf(
    Spring(
        nameId = R.string.spring_safarne_name,
        descriptionId = R.string.spring_safarne_description,
        geoLocationUriId = R.string.spring_safarne_geo_uri,
        imageResourceId = R.drawable.img_safarne,
    ),
)

val springsSakiai = listOf(
    Spring(
        nameId = R.string.spring_lekeciai_name,
        descriptionId = R.string.spring_lekeciai_description,
        geoLocationUriId = R.string.spring_lekeciai_geo_uri,
        imageResourceId = R.drawable.img_lekeciai,
    ),
)

val springsValkininkai = listOf(
    Spring(
        nameId = R.string.spring_spengla_name,
        descriptionId = R.string.spring_spengla_description,
        geoLocationUriId = R.string.spring_spengla_geo_uri,
        imageResourceId = R.drawable.img_spengla,
    ),
)

val springsPrienai = listOf(
    Spring(
        nameId = R.string.spring_islauzas_name,
        descriptionId = R.string.spring_islauzas_description,
        geoLocationUriId = R.string.spring_islauzas_geo_uri,
        imageResourceId = R.drawable.img_islauzas,
    ),
)

val springsGoa = listOf(
    Spring(
        nameId = R.string.spring_salmona_name,
        descriptionId = R.string.spring_salmona_description,
        geoLocationUriId = R.string.spring_salmona_geo_uri,
        imageResourceId = R.drawable.img_salmona,
    ),
    Spring(
        nameId = R.string.spring_oxel_name,
        descriptionId = R.string.spring_oxel_description,
        geoLocationUriId = R.string.spring_oxel_geo_uri,
        imageResourceId = R.drawable.img_oxel,
    ),
    Spring(
        nameId = R.string.spring_siolim_name,
        descriptionId = R.string.spring_siolim_description,
        geoLocationUriId = R.string.spring_siolim_geo_uri,
        imageResourceId = R.drawable.img_siolim,
    ),
    Spring(
        nameId = R.string.spring_vagator_name,
        descriptionId = R.string.spring_vagator_description,
        geoLocationUriId = R.string.spring_vagator_geo_uri,
        imageResourceId = R.drawable.img_vagator,
    ),
)

val springsVarkala = listOf(
    Spring(
        nameId = R.string.spring_varkala_name,
        descriptionId = R.string.spring_varkala_description,
        geoLocationUriId = R.string.spring_varkala_geo_uri,
        imageResourceId = R.drawable.img_varkala,
    ),
)

val springsMuvattupuzha = listOf(
    Spring(
        nameId = R.string.spring_nakapuzha_name,
        descriptionId = R.string.spring_nakapuzha_description,
        geoLocationUriId = R.string.spring_nakapuzha_geo_uri,
        imageResourceId = R.drawable.img_nakapuzha,
    ),
)

val springsGalle = listOf(
    Spring(
        nameId = R.string.spring_jungle_beach_name,
        descriptionId = R.string.spring_jungle_beach_description,
        geoLocationUriId = R.string.spring_jungle_beach_geo_uri,
        imageResourceId = R.drawable.img_jungle_beach,
    ),
    Spring(
        nameId = R.string.spring_meadow_name,
        descriptionId = R.string.spring_meadow_description,
        geoLocationUriId = R.string.spring_meadow_geo_uri,
        imageResourceId = R.drawable.img_meadow,
    ),
    Spring(
        nameId = R.string.spring_jackfruit_name,
        descriptionId = R.string.spring_jackfruit_description,
        geoLocationUriId = R.string.spring_jackfruit_geo_uri,
        imageResourceId = R.drawable.img_jackfruit,
    ),
)

val springsDikwella = listOf(
    Spring(
        nameId = R.string.spring_harbour_name,
        descriptionId = R.string.spring_harbour_description,
        geoLocationUriId = R.string.spring_harbour_geo_uri,
        imageResourceId = R.drawable.img_harbour,
    ),
    Spring(
        nameId = R.string.spring_nilwella_beach_name,
        descriptionId = R.string.spring_nilwella_beach_description,
        geoLocationUriId = R.string.spring_nilwella_beach_geo_uri,
        imageResourceId = R.drawable.img_nilwella_beach,
    ),
)

val springsTrincomalee = listOf(
    Spring(
        nameId = R.string.spring_kanniya_name,
        descriptionId = R.string.spring_kanniya_description,
        geoLocationUriId = R.string.spring_kanniya_geo_uri,
        imageResourceId = R.drawable.img_kanniya,
    ),
)

val springsKandy = listOf(
    Spring(
        nameId = R.string.spring_hanthana_name,
        descriptionId = R.string.spring_hanthana_description,
        geoLocationUriId = R.string.spring_hanthana_geo_uri,
        imageResourceId = R.drawable.img_hanthana,
    ),
)

val springsMirissa = listOf(
    Spring(
        nameId = R.string.spring_secret_beach_name,
        descriptionId = R.string.spring_secret_beach_description,
        geoLocationUriId = R.string.spring_secret_beach_geo_uri,
        imageResourceId = R.drawable.img_secret_beach,
    ),
)

val springsElla = listOf(
    Spring(
        nameId = R.string.spring_ravana_name,
        descriptionId = R.string.spring_ravana_description,
        geoLocationUriId = R.string.spring_ravana_geo_uri,
        imageResourceId = R.drawable.img_ravana,
    ),
    Spring(
        nameId = R.string.spring_highway_name,
        descriptionId = R.string.spring_highway_description,
        geoLocationUriId = R.string.spring_highway_geo_uri,
        imageResourceId = R.drawable.img_highway,
    ),
    Spring(
        nameId = R.string.spring_cave_temple_name,
        descriptionId = R.string.spring_cave_temple_description,
        geoLocationUriId = R.string.spring_cave_temple_geo_uri,
        imageResourceId = R.drawable.img_cave_temple,
    ),
    Spring(
        nameId = R.string.spring_riversplendor_name,
        descriptionId = R.string.spring_riversplendor_description,
        geoLocationUriId = R.string.spring_riversplendor_geo_uri,
        imageResourceId = R.drawable.img_riversplendor,
    ),
)

val springsKualaLumpur = listOf(
    Spring(
        nameId = R.string.spring_bukit_besi_name,
        descriptionId = R.string.spring_bukit_besi_description,
        geoLocationUriId = R.string.spring_bukit_besi_geo_uri,
        imageResourceId = R.drawable.img_bukit_besi,
    ),
    Spring(
        nameId = R.string.spring_bukit_kiara_name,
        descriptionId = R.string.spring_bukit_kiara_description,
        geoLocationUriId = R.string.spring_bukit_kiara_geo_uri,
        imageResourceId = R.drawable.img_bukit_kiara,
    ),
    Spring(
        nameId = R.string.spring_fern_name,
        descriptionId = R.string.spring_fern_description,
        geoLocationUriId = R.string.spring_fern_geo_uri,
        imageResourceId = R.drawable.img_fern,
    ),
    Spring(
        nameId = R.string.spring_selayang_name,
        descriptionId = R.string.spring_selayang_description,
        geoLocationUriId = R.string.spring_selayang_geo_uri,
        imageResourceId = R.drawable.img_selayang,
    ),
)

val springsPenang = listOf(
    Spring(
        nameId = R.string.spring_cangkat_name,
        descriptionId = R.string.spring_cangkat_description,
        geoLocationUriId = R.string.spring_cangkat_geo_uri,
        imageResourceId = R.drawable.img_cangkat,
    ),
    Spring(
        nameId = R.string.spring_freedom_name,
        descriptionId = R.string.spring_freedom_description,
        geoLocationUriId = R.string.spring_freedom_geo_uri,
        imageResourceId = R.drawable.img_freedom,
    ),
    Spring(
        nameId = R.string.spring_gold_name,
        descriptionId = R.string.spring_gold_description,
        geoLocationUriId = R.string.spring_gold_geo_uri,
        imageResourceId = R.drawable.img_gold,
    ),
    Spring(
        nameId = R.string.spring_carla_name,
        descriptionId = R.string.spring_carla_description,
        geoLocationUriId = R.string.spring_carla_geo_uri,
        imageResourceId = R.drawable.img_carla,
    ),
    Spring(
        nameId = R.string.spring_bungah_name,
        descriptionId = R.string.spring_bungah_description,
        geoLocationUriId = R.string.spring_bungah_geo_uri,
        imageResourceId = R.drawable.img_bungah,
    ),
    Spring(
        nameId = R.string.spring_monitor_name,
        descriptionId = R.string.spring_monitor_description,
        geoLocationUriId = R.string.spring_monitor_geo_uri,
        imageResourceId = R.drawable.img_monitor,
    ),
)

val springsTioman = listOf(
    Spring(
        nameId = R.string.spring_bamboo_name,
        descriptionId = R.string.spring_bamboo_description,
        geoLocationUriId = R.string.spring_bamboo_geo_uri,
        imageResourceId = R.drawable.img_bamboo,
    ),
)

val springsPhuket = listOf(
    Spring(
        nameId = R.string.spring_patong_hill_name,
        descriptionId = R.string.spring_patong_hill_description,
        geoLocationUriId = R.string.spring_patong_hill_geo_uri,
        imageResourceId = R.drawable.img_patong_hill,
    ),
    Spring(
        nameId = R.string.spring_wiset_name,
        descriptionId = R.string.spring_wiset_description,
        geoLocationUriId = R.string.spring_wiset_geo_uri,
        imageResourceId = R.drawable.img_wiset,
    ),
    Spring(
        nameId = R.string.spring_sae_name,
        descriptionId = R.string.spring_sae_description,
        geoLocationUriId = R.string.spring_sae_geo_uri,
        imageResourceId = R.drawable.img_sae,
    ),
)

val springsPhangan = listOf(
    Spring(
        nameId = R.string.spring_srithanu_name,
        descriptionId = R.string.spring_srithanu_description,
        geoLocationUriId = R.string.spring_srithanu_geo_uri,
        imageResourceId = R.drawable.img_srithanu,
    ),
    Spring(
        nameId = R.string.spring_zoo_name,
        descriptionId = R.string.spring_zoo_description,
        geoLocationUriId = R.string.spring_zoo_geo_uri,
        imageResourceId = R.drawable.img_zoo,
    ),
    Spring(
        nameId = R.string.spring_recess_name,
        descriptionId = R.string.spring_recess_description,
        geoLocationUriId = R.string.spring_recess_geo_uri,
        imageResourceId = R.drawable.img_recess,
    ),
    Spring(
        nameId = R.string.spring_haadyao_name,
        descriptionId = R.string.spring_haadyao_description,
        geoLocationUriId = R.string.spring_haadyao_geo_uri,
        imageResourceId = R.drawable.img_haadyao,
    ),
    Spring(
        nameId = R.string.spring_washout_name,
        descriptionId = R.string.spring_washout_description,
        geoLocationUriId = R.string.spring_washout_geo_uri,
        imageResourceId = R.drawable.img_washout,
    ),
    Spring(
        nameId = R.string.spring_jade_name,
        descriptionId = R.string.spring_jade_description,
        geoLocationUriId = R.string.spring_jade_geo_uri,
        imageResourceId = R.drawable.img_jade,
    ),
    Spring(
        nameId = R.string.spring_ridge_name,
        descriptionId = R.string.spring_ridge_description,
        geoLocationUriId = R.string.spring_ridge_geo_uri,
        imageResourceId = R.drawable.img_ridge,
    ),
)

val springsSingapore = listOf(
    Spring(
        nameId = R.string.spring_sembawang_name,
        descriptionId = R.string.spring_sembawang_description,
        geoLocationUriId = R.string.spring_sembawang_geo_uri,
        imageResourceId = R.drawable.img_sembawang,
    ),
    Spring(
        nameId = R.string.spring_shelter_name,
        descriptionId = R.string.spring_shelter_description,
        geoLocationUriId = R.string.spring_shelter_geo_uri,
        imageResourceId = R.drawable.img_shelter,
    ),
)

val springsHongKong = listOf(
    Spring(
        nameId = R.string.spring_hatton_name,
        descriptionId = R.string.spring_hatton_description,
        geoLocationUriId = R.string.spring_hatton_geo_uri,
        imageResourceId = R.drawable.img_hatton,
    ),
    Spring(
        nameId = R.string.spring_lugard_name,
        descriptionId = R.string.spring_lugard_description,
        geoLocationUriId = R.string.spring_lugard_geo_uri,
        imageResourceId = R.drawable.img_lugard,
    ),
    Spring(
        nameId = R.string.spring_hospital_name,
        descriptionId = R.string.spring_hospital_description,
        geoLocationUriId = R.string.spring_hospital_geo_uri,
        imageResourceId = R.drawable.img_hospital,
    ),
)

val springsMacau = listOf(
    Spring(
        nameId = R.string.spring_lakeside_name,
        descriptionId = R.string.spring_lakeside_description,
        geoLocationUriId = R.string.spring_lakeside_geo_uri,
        imageResourceId = R.drawable.img_lakeside,
    ),
    Spring(
        nameId = R.string.spring_situ_name,
        descriptionId = R.string.spring_situ_description,
        geoLocationUriId = R.string.spring_situ_geo_uri,
        imageResourceId = R.drawable.img_situ,
    ),
    Spring(
        nameId = R.string.spring_observation_name,
        descriptionId = R.string.spring_observation_description,
        geoLocationUriId = R.string.spring_observation_geo_uri,
        imageResourceId = R.drawable.img_observation,
    ),
    Spring(
        nameId = R.string.spring_cabrita_name,
        descriptionId = R.string.spring_cabrita_description,
        geoLocationUriId = R.string.spring_cabrita_geo_uri,
        imageResourceId = R.drawable.img_cabrita,
    ),
)

val springsPhuQuoc = listOf(
    Spring(
        nameId = R.string.spring_tranh_name,
        descriptionId = R.string.spring_tranh_description,
        geoLocationUriId = R.string.spring_tranh_geo_uri,
        imageResourceId = R.drawable.img_tranh,
    ),
    Spring(
        nameId = R.string.spring_buckle_name,
        descriptionId = R.string.spring_buckle_description,
        geoLocationUriId = R.string.spring_buckle_geo_uri,
        imageResourceId = R.drawable.img_buckle,
    ),
)

val locations = listOf(
    Location(R.string.location_vilnius, Country.Code.LT, springsVilnius),
    Location(R.string.location_kaunas, Country.Code.LT, springsKaunas),
    Location(R.string.location_taurage, Country.Code.LT, springsTaurage),
    Location(R.string.location_jurbarkas, Country.Code.LT,springsJurbarkas),
    Location(R.string.location_seredzius, Country.Code.LT, springsSeredzius),
    Location(R.string.location_prienai, Country.Code.LT, springsPrienai),
    Location(R.string.location_sakiai, Country.Code.LT, springsSakiai),
    Location(R.string.location_aukstadvaris, Country.Code.LT, springsAukstadvaris),
    Location(R.string.location_valkininkai, Country.Code.LT, springsValkininkai),
    Location(R.string.location_goa, Country.Code.IN, springsGoa),
    Location(R.string.location_varkala, Country.Code.IN, springsVarkala),
    Location(R.string.location_muvattupuzha, Country.Code.IN, springsMuvattupuzha),
    Location(R.string.location_trincomalee, Country.Code.LK, springsTrincomalee),
    Location(R.string.location_kandy, Country.Code.LK, springsKandy),
    Location(R.string.location_mirissa, Country.Code.LK, springsMirissa),
    Location(R.string.location_galle, Country.Code.LK, springsGalle),
    Location(R.string.location_dikwella, Country.Code.LK, springsDikwella),
    Location(R.string.location_ella, Country.Code.LK, springsElla),
    Location(R.string.location_kuala_lumpur, Country.Code.MY, springsKualaLumpur),
    Location(R.string.location_penang, Country.Code.MY, springsPenang),
    Location(R.string.location_tioman, Country.Code.MY, springsTioman),
    Location(R.string.location_phuket, Country.Code.TH, springsPhuket),
    Location(R.string.location_phangan, Country.Code.TH, springsPhangan),
    Location(R.string.location_singapore, Country.Code.SG, springsSingapore),
    Location(R.string.location_hong_kong, Country.Code.CN, springsHongKong),
    Location(R.string.location_macau, Country.Code.CN, springsMacau),
    Location(R.string.location_phu_quoc, Country.Code.VN, springsPhuQuoc),
)