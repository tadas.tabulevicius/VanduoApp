/*
 * Vanduo app to locate water springs.
 * Copyright© 2023-2024
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

package lt.vanduo.app.springs

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.launch
import lt.vanduo.app.utils.Country

class SpringsViewModel(private val country: Country) : ViewModel() {
    private val filteredLocations = filterLocationsByCountry(country.getSelectedCode())
    private val springsScreenData = SpringsScreenData(filteredLocations)
    private val _dataFlow = MutableStateFlow(springsScreenData)
    val dataFlow: StateFlow<SpringsScreenData> = _dataFlow.asStateFlow()

    init {
        viewModelScope.launch {
            country.selectedCountryCodeFlow.collect { selectedCountry ->
                emitFilteredSprings(selectedCountry)
            }
        }
    }

    private fun emitFilteredSprings(selectedCountry: Country.Code) {
        val filteredLocations = filterLocationsByCountry(selectedCountry)
        _dataFlow.tryEmit(SpringsScreenData(filteredLocations))
    }
    private fun filterLocationsByCountry(selectedCountry: Country.Code) = when (selectedCountry) {
        Country.Code.ALL -> locations
        else -> locations.filter { it.country == selectedCountry }
    }
}

class SpringsViewModelFactory(private val country: Country) : ViewModelProvider.Factory {
    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(SpringsViewModel::class.java)) {
            return SpringsViewModel(country) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}

data class SpringsScreenData(val springLocations: List<Location>)




