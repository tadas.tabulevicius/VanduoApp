/*
 * Vanduo app to locate water springs.
 * Copyright© 2023-2024
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

package lt.vanduo.app.springs

import androidx.annotation.StringRes
import androidx.compose.foundation.BorderStroke
import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.alpha
import androidx.compose.ui.draw.clip
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import lt.vanduo.app.R
import lt.vanduo.app.ui.theme.Blue

@OptIn(ExperimentalFoundationApi::class)
@Composable
fun SpringsScreen(
    springsViewModel: SpringsViewModel,
    navigateToSpringDetailScreen: (Spring) -> Unit,
    navigateToInfoScreen: () -> Unit,
) {
    val springsScreenData = springsViewModel.dataFlow.collectAsState()
    Surface(
        modifier = Modifier.fillMaxSize(),
        color = MaterialTheme.colorScheme.background,
    ) {
        LazyColumn(
            verticalArrangement = Arrangement.spacedBy(16.dp),
            horizontalAlignment = Alignment.CenterHorizontally,
            modifier = Modifier
                .padding(bottom = 16.dp),
        ) {
            stickyHeader {
                InfoButton(navigateToInfoScreen)
            }
            for (location in springsScreenData.value.springLocations) {
                stickyHeader {
                    LocationHeader(locationNameId = location.nameId)
                }
                items(items = location.springs) {
                    SpringCard(
                        it,
                        { navigateToSpringDetailScreen(it) }
                    )
                }
            }
        }
    }
}

@Composable
private fun LocationHeader(@StringRes locationNameId: Int) {
    Text(
        text = stringResource(locationNameId),
        modifier = Modifier
            .background(color = Blue)
            .fillMaxSize()
            .padding(8.dp),
        style = MaterialTheme.typography.titleMedium,
    )
}

@Composable
private fun InfoButton(navigateToInfoScreen: () -> Unit) {
    Box(
        contentAlignment = Alignment.Center,
        modifier = Modifier
            .fillMaxWidth()
            .background(Blue)
            .padding(
                start = 0.dp,
                top = 8.dp,
                end = 0.dp,
                bottom = 8.dp,
            )
    ) {
        Image(
            painter = painterResource(R.drawable.ic_triangle_b_g_512),
            contentDescription = "avatar",
            contentScale = ContentScale.Crop,
            modifier = Modifier
                .size(48.dp)
                .clip(CircleShape)
                .clickable { navigateToInfoScreen() }
        )
    }
}

@Composable
fun SpringCard(
    spring: Spring,
    onOptionSelected: () -> Unit,
    modifier: Modifier = Modifier,
) {
    Surface(
        shape = MaterialTheme.shapes.small,
        color = MaterialTheme.colorScheme.surface,
        border = BorderStroke(
            width = 1.dp,
            color = MaterialTheme.colorScheme.outline
        ),
        modifier = modifier
            .padding(horizontal = 8.dp)
            .clip(MaterialTheme.shapes.small)
            .clickable(onClick = onOptionSelected)
    ) {
        spring.imageResourceId?.let {
            Image(
                painter = painterResource(id = it),
                contentDescription = null,
                modifier = Modifier.alpha(0.5F)
            )
        }
        Column(
            modifier = Modifier
                .fillMaxWidth()
                .padding(16.dp)
        ) {
            Text(
                text = stringResource(spring.nameId),
                modifier = Modifier.padding(0.dp, 0.dp, 0.dp, 16.dp),
                style = MaterialTheme.typography.titleLarge,

                )
            Text(
                text = stringResource(spring.descriptionId),
                style = MaterialTheme.typography.bodyLarge
            )

        }
    }
}

@Preview
@Composable
fun SpringPreview() {
    SpringCard(spring = springsVilnius[0], onOptionSelected = { })
}

