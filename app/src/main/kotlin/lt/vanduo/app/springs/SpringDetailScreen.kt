/*
 * Vanduo app to locate water springs.
 * Copyright© 2023-2024
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

package lt.vanduo.app.springs

import android.content.Context
import android.content.Intent
import android.net.Uri
import androidx.compose.foundation.Image
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import lt.vanduo.app.R

@Composable
fun SpringDetailScreen(
    springNameId: Int?,
) {
    val spring: Spring = locations.flatMap { it.springs }
        .firstOrNull { it.nameId == springNameId } ?: locations[0].springs[0]
    Surface(
        modifier = Modifier.fillMaxSize(),
        color = MaterialTheme.colorScheme.background,
    ) {
        val context = LocalContext.current
        Column(
            verticalArrangement = Arrangement.spacedBy(16.dp),
            horizontalAlignment = Alignment.CenterHorizontally,
        ) {

            Text(
                text = stringResource(spring.nameId),
                modifier = Modifier.padding(0.dp, 32.dp, 0.dp, 16.dp),
                style = MaterialTheme.typography.headlineSmall,

                )
            spring.imageResourceId?.let {
                Image(
                    painter = painterResource(id = it),
                    contentDescription = null,
                )
            }
            Text(
                text = stringResource(spring.descriptionId),
                style = MaterialTheme.typography.bodyLarge,
                textAlign = TextAlign.Justify,
                modifier = Modifier.padding(24.dp),
            )
            Image(
                painter = painterResource(id = R.drawable.ic_location_48),
                contentDescription = null,
                modifier = Modifier.clickable(
                    onClick = { showMap(context = context, spring.geoLocationUriId) }
                )
            )
        }
    }
}

private fun showMap(context: Context, geoUriResourceId: Int) {
    val intent = Intent(Intent.ACTION_VIEW)
    intent.data = Uri.parse(context.getString(geoUriResourceId))
    if (intent.resolveActivity(context.packageManager) != null) {
        context.startActivity(intent)
    }
}

