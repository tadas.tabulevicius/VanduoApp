package lt.vanduo.app.ui.theme

import androidx.compose.ui.graphics.Color

val Blue = Color(0xFF297AB9)
val Green = Color(0xFFBAE33D)