/*
 * Vanduo app to locate water springs.
 * Copyright© 2023-2024
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

package lt.vanduo.app.utils

import android.app.Activity
import android.content.res.Configuration
import android.content.res.Resources
import io.mockk.every
import io.mockk.just
import io.mockk.mockk
import io.mockk.mockkConstructor
import io.mockk.runs
import io.mockk.slot
import io.mockk.unmockkConstructor
import io.mockk.verify
import org.junit.After
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test
import java.util.Locale

class LanguageTest {
    private lateinit var language: Language
    private val localeSlot = slot<Locale>()
    private val configurationMock: Configuration = mockk {
        every { setLocale(capture(localeSlot)) } just runs
    }

    private val resourcesMock: Resources = mockk(relaxed = true) {
        every { configuration } returns configurationMock
    }

    private val activityMock: Activity = mockk(relaxed = true) {
        every { recreate() } just runs
        every { resources } returns resourcesMock
    }


    private val languageCodeSlot = slot<String>()
    private val preferencesMockk: Preferences = mockk {
        every { setLanguage(capture(languageCodeSlot)) } just runs
    }

    @Before
    fun setUp() {
        localeSlot.clear()
        languageCodeSlot.clear()

        mockkConstructor(Configuration::class)
        every {
            anyConstructed<Configuration>().setLocale(capture(localeSlot))
        } just runs

        language = Language(activityMock, preferencesMockk)
    }

    @After
    fun tearDown() {
        unmockkConstructor(Configuration::class)
    }

    @Test
    fun `when check() is called then Preferences language is selected`() {
        every { preferencesMockk.getLanguage() } returns "lt"

        language.check()

        assertEquals("lt", localeSlot.captured.language)
    }

    @Test
    fun `when check() is called and Preferences language is NOT set then language is set to default`() {
        every { preferencesMockk.getLanguage() } returns ""

        language.check()

        assertEquals("en", localeSlot.captured.language)
    }

    @Test
    fun `when check() is called with unknown language in Preferences then default language is set`() {
        val isoNameUA = "ua"
        every { preferencesMockk.getLanguage() } returns isoNameUA
        Locale.setDefault(Locale(isoNameUA))

        language.check()

        assertEquals("en", localeSlot.captured.language)
    }

    @Test
    fun `when check() is called with unknown language in Preferences then default language saved to Preferences`() {
        val isoNameUA = "ua"
        every { preferencesMockk.getLanguage() } returns isoNameUA
        Locale.setDefault(Locale(isoNameUA))

        language.check()

        assertEquals("en", languageCodeSlot.captured)
    }

    @Test
    fun `when getDefaultCode() is called with default locale set to LT then return LT code`() {
        val isoNameLT = "lt"
        Locale.setDefault(Locale(isoNameLT))

        val code = language.getDefaultCode()

        assertEquals(isoNameLT, code.isoName)
    }

    @Test
    fun `when getDefaultCode() is called with default locale set to EN then return EN code`() {
        val isoNameEN = "en"
        Locale.setDefault(Locale(isoNameEN))

        val code = language.getDefaultCode()

        assertEquals(isoNameEN, code.isoName)
    }

    @Test
    fun `when getDefaultCode() is called with unknown default locale then return EN code`() {
        Locale.setDefault(Locale("ua"))

        val code = language.getDefaultCode()

        assertEquals("en", code.isoName)
    }

    @Test
    fun `when select() is called then language is applied`() {
        Locale.setDefault(Locale("en"))

        language.select(Language.Code.LT)

        assertEquals("lt", localeSlot.captured.language)
    }

    @Test
    fun `when select() is called then language is saved to Preferences`() {
        language.select(Language.Code.LT)

        assertEquals("lt", languageCodeSlot.captured)
    }

    @Test
    fun `when select() is called and default language matches then NO action is performed`() {
        Locale.setDefault(Locale("lt"))

        language.select(Language.Code.LT)

        verify(exactly = 0) { activityMock.recreate() }
    }
}